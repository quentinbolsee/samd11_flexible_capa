//
// d11_flex_freetouch.ino
//
// Quentin Bolsee 2022-10-31
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

#include "Adafruit_FreeTouch.h"

#define PIN_CAPA 4
#define N_LED 4
#define VALUE_INCR (1023/N_LED)
#define N_LOOKUP 6

#define DEBUG

int pins_led[N_LED] = {5, 8, 9, 14};

// results may vary!
uint32_t lookup_x[N_LOOKUP] = {440, 447, 456, 487, 516, 551};
uint32_t lookup_y[N_LOOKUP] = {0,  342,  584,  868,  980, 1023};

Adafruit_FreeTouch qt(PIN_CAPA, OVERSAMPLE_4, RESISTOR_50K, FREQ_MODE_NONE);

void setup() {
  //SerialUSB.begin(0);
  for (int i = 0; i < N_LED; i++) {
    pinMode(pins_led[i], OUTPUT);
    analogWrite(pins_led[i], 0);
  }
  qt.begin();
}


int linearize(int reading) {
  if (reading <= lookup_x[0]) {
    return 0;
  }

  if (reading >= lookup_x[N_LOOKUP-1]) {
    return 1023;
  }

  int k = 0;
  while (k < N_LOOKUP-2) {
    if (reading >= lookup_x[k] && reading <= lookup_x[k+1]) {
      break;
    }
    k++;
  }

  uint32_t dx = lookup_x[k+1] - lookup_x[k];
  uint32_t diff = reading - lookup_x[k];

  return lookup_y[k] + diff * (lookup_y[k+1] - lookup_y[k]) / dx;
}


void show_level(int level) {
  // level is 0-1023
  unsigned long i_lit = min(N_LED-1, level/VALUE_INCR);

  for (int i = 0; i < N_LED; i++) {
    analogWrite(pins_led[i], i < i_lit ? 255 : 0);
  }
  unsigned long diff = level - i_lit * VALUE_INCR;
  diff = max(min(VALUE_INCR, diff), 0);
  analogWrite(pins_led[i_lit], min((diff * 255UL) / VALUE_INCR, 255));
}

void loop() {
  int qt_value = qt.measure();

  int v = linearize(qt_value);

  show_level(v);

  #ifdef DEBUG
    SerialUSB.println(qt_value);
    delay(50);
  #else
    delay(1);
  #endif
}
