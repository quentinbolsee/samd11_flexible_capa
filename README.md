# SAMD11 Flexible Capacitor

This project demonstrates qualitative angle/force reading using the peripheral touch controller (PTC) of a SAMD11 on a flexural capacitance pad in front of a ground plane.

<img src=video/flex.mp4 width=50%><br>

The copper on top of the flexural joint allows the ground plane to be connected despite the flexing. On a FR1 board, the bend seems reliable and durable.

[![](img/flexing1.jpg)](img/flexing1.jpg)

## Design

### Schematic

The design is as simple as it gets: a 3.3V regulator feeding the D11, a 2x2 SWD connector for flashing and a set of 4 LED + resistor for visual feedback. The capacitance pad is merely a tag, and has to be manually added in the design as a copper pour.

[![](img/schematic.png)](img/schematic.png)

### Board layout

Making the flexure in KiCAD was tedious as it involved copy/pasting one flexure element and lining it up perfectly on the grid.

[![](img/board.png)](img/board.png)

Here are subtleties in the design:

- The GND pad (top) and capacitance pad are kept separated as much as possible, otherwise the capacitance at rest would become non-negligible
- Cleared copper zones on the top and are meant to hold the flexure with fingers without impacting the capacitance readings.

## Making

### Milling

The milling was performed in around 30 minutes with the following operations:

- Two isolation passes with a 0.4mm flat end mill (depth 0.1mm)
- Cut of the flexure with a 0.4mm mill (depth 1.6mm, steps of 0.25mm)
- Copper clearing pass with a 0.8mm flat end mill (depth 0.1mm)
- Edge cute with a 0.8mm mill (depth 1.6mm, steps of 0.45mm)

[![](img/mill.jpg)](img/mill.jpg)

### Soldering

The finished board shows no unwanted copper thanks to the clearing pass with a 0.8mm mill. The board is cleaned with soap and water and thoroughly dried before soldering.

[![](img/soldering.jpg)](img/soldering.jpg)

I install the SAMD11 first as it has the tighted pin pich among all components. The last components I add are the LEDs and resistors.

[![](img/soldered.jpg)](img/soldered.jpg)

## Flexure

Bending the flexure is notn only fun, it also seems reliable on the long run. I suspect that a FR4 board wouldn't be as compliant as the FR1 I've used here.

[![](img/flexing1.jpg)](img/flexing1.jpg)

The bend can reache a full 180. Note that I avoid touching or hovering the pads with my fingers, as this would affect the reading.

[![](img/flexing2.jpg)](img/flexing2.jpg)

## Code

The code has three main purposes:

- Setting up the PTC for reading the capacitance continuously
- Sending the values to the host computer through the CDC serial, for furhter analysis
- Lighting up the LEDs for a fun visualization of the capacitance's reading. For a nicer effect, PWM is used to modulate the current LED in the gauge. When it's fully lit, the next LED is enabled, etc.

The full code is available [here](code/d11_flex_freetouch.ino).

It makes use of the [Fab SAM Arduino Core](https://github.com/qbolsee/ArduinoCore-fab-sam), but without a bootloader to save 4kb of flash. The binary is compiled in the IDE with "NO_BOOTLOADER" under the setting "Bootloader size". The compiled binary can then be flashed using a CMSIS-DAP adapter, for instance using edbg:

```
edbg -ebpv -t samd21 -f d11_flex_freetouch.ino.bin
```

### Adafruit Freetouch

The code makes intensive use of the Adafruit Freetouch library, which is a clever reverse engineering of the Qtouch hardware included in the SAMD family. The library must be installed separately, in this case you need my modified fork available [here](https://github.com/qbolsee/Adafruit_FreeTouch/).

## Signal processing

The raw signal is a 10 bit value, that increases with the capacitance. It saturates at 1023 whenever a finger or the other pad is touching.

<img src=video/signal.mp4 width=50%><br>

By flexing at a constant rate, I was able to extract a very crude curve.

[![](img/signal_capture.jpg)](img/signal_capture.jpg)

For the purpose of this project, this was enough to provide a lookup table with 6 samples:

```
uint32_t lookup_x[] = {440, 447, 456, 487, 516, 551};
uint32_t lookup_y[] = {0,  342,  584,  868,  980, 1023};
```

By interpolating this lookup table based on a reading, a linearized 10 bit value is provided, and the LEDs are lit up according to that converted value.

## Demo

<img src=video/flex.mp4 width=50%><br>

## Downloads

### PNG files (1000 DPI)

Traces:

<img src=design/png/samd11c14_flex_gauge-F_Cu.png width=50%><br>

Interior:

<img src=design/png/samd11c14_flex_gauge-Edge_Cuts.png width=50%><br>

Traces + exterior:

<img src=design/png/samd11c14_flex_gauge-interior_traces_comp.png width=50%><br>

Composite:

<img src=design/png/samd11c14_flex_gauge-composite.png width=50%><br>

### KiCAD 6 files

- [D11_flexible_capa.zip](design/D11_flexible_capa.zip)

### Firmware

- [d11_flex_freetouch.ino.bin](binary/d11_flex_freetouch.ino.bin)

### Code

The code is available [here](code/d11_flex_freetouch.ino).

## License

This project is provided under the MIT License.
